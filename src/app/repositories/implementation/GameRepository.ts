import { IGameRepository } from '../interface/IGameRepository'
import { Game } from '../../entities/Game'
import { getRepository } from 'typeorm'

export class GameRepository implements IGameRepository {
    create = async (entity: Game) => {
        try{
            var newGame = await getRepository(Game).create(entity)
            await getRepository(Game).save(newGame)
        }catch(err){
            console.error('err: ', err)
            return false
        }

        return true
    }
    findById = async (id: number) => {
        var resultado = new Game()
        try{
            resultado = await getRepository(Game).findOneOrFail(id)
        }catch(err){
            console.error('err: ', err)
            return resultado
        }

        return resultado
    }
    findAll = async () => {
        var resultado = new Array<Game>()
        try{
            resultado = await getRepository(Game).find()
        }catch(err){
            console.error('err: ', err)
            return resultado
        }

        return resultado
    }
    update = async (entity: Game) => {
        return true
    }
    delete = async (entity: Game) => {
        return true
    }

}