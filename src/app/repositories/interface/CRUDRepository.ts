interface CRUDRepository<T>{
    create: (entity: T) => Promise<boolean>
    findById:(id: number ) => Promise<T>
    findAll: () => Promise<Array<T>>
    update: (entity: T) => Promise<boolean>
    delete: (entity: T) => Promise<boolean>
}