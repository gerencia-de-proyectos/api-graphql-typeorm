import { Game } from '../../entities/Game'

export interface IGameRepository extends CRUDRepository<Game>{
    
}
