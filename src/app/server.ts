import express from 'express'
import {PORT_APP} from '../config/constant'
import {ApolloServer} from 'apollo-server-express'
import {buildSchema} from 'type-graphql'

import {GameResolver} from './resolvers/GameResolver'

const startServer = async() => {
    const app = express()
    const server = new ApolloServer({
        schema: await buildSchema({
            resolvers: [GameResolver]
        }),
        context: ({req, res}) => ({req, res})
    })   

    app.set('port',PORT_APP)
    server.applyMiddleware({app, path: '/api'})

    return app
}

export {
    startServer
}