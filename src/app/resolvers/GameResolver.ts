import { GameRepository } from './../repositories/implementation/GameRepository';
import { Resolver, Query, Mutation, Arg, Field, InputType } from 'type-graphql'
import { Game } from '../entities/Game'
// import { Game } from '../entities/Game'

@InputType()
class GameInput extends Game{
    @Field()
    name!: string;
    @Field()
    picture!: string;
}

@Resolver()
class GameResolver{
    gameRepository: GameRepository
    constructor(){
        this.gameRepository = new GameRepository()
    }

    @Query( returns => [Game])
    games(){
        return this.gameRepository.findAll()
    }

    @Query( returns => Game)
    gameById(
        @Arg("id") id: number
    ){
        return this.gameRepository.findById(id)
    }

    @Mutation( returns => Boolean)
    async createGame(
        @Arg("game", ()=>GameInput) game: GameInput,
    ){
        return this.gameRepository.create(game)
    }
}

export {
    GameResolver
}