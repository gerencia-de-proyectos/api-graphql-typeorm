import { Entity, BaseEntity, PrimaryGeneratedColumn, Column} from 'typeorm'
import { ObjectType, Field, Int } from 'type-graphql'

@ObjectType()
@Entity("Game")
export class Game extends BaseEntity{

    @Field(()=>Int)
    @PrimaryGeneratedColumn()
    id!: number;

    @Field(()=>String)
    @Column()
    name!: string;

    @Field()
    @Column()
    picture!: string;
}