import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn} from 'typeorm'
import { ObjectType, Field, Int } from 'type-graphql'

@ObjectType()
@Entity("Player")
export class Player extends BaseEntity{

    @Field(()=>Int)
    @PrimaryGeneratedColumn()
    id!: number;

    @Field(()=>String)
    @Column()
    nick!: string;

    @Field(()=>String)
    @Column()
    img_profile!: string;

    @Field(()=> String)
    @Column()
    first_name!: string;
    
    @Field(()=> String)
    @Column()
    last_name!: string;

    @Field(()=> String)
    @Column()
    email!: string;

    @Field()
    @Column()
    birthday!: Date;
    
    @Field(()=> String)
    @CreateDateColumn({type: 'timestamp'})
    date_created!: string;

    
}