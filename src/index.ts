import "reflect-metadata"
import {startServer} from './app/server'
import {createConnection} from 'typeorm'

async function main() {
    await createConnection()
    const app = await startServer()
    app.listen(app.get('port'))
    console.log('Server on port', app.get('port'))
}

main()